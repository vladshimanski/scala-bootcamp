package com.evolution.scala.bootcamp.hw3

import ControlStructures.Command._

import scala.io.Source
import scala.util.{Failure, Success, Try}

object ControlStructures {

  // Homework

  // Create a command line application that reads various "commands" from the
  // stdin, evaluates them, and writes output to stdout.

  // Commands are:

  //   divide 4 5
  // which should output "4 divided by 5 is 0.8"

  //   sum 5 5 6 8.5
  // which should output "the sum of 5 5 6 8.5 is 24.5"

  //   average 4 3 8.5 4
  // which should output "the average of 4 3 8.5 4 is 4.875"

  //   min 4 -3 -17
  // which should output "the minimum of 4 -3 -17 is -17"

  //   max 4 -3 -17
  // which should output "the maximum of 4 -3 -17 is 4"

  // In case of commands that cannot be parsed or calculations that cannot be performed,
  // output a single line starting with "Error: "

  sealed trait Command

  object Command {

    final case class Divide(dividend: Double, divisor: Double) extends Command

    final case class Sum(numbers: List[Double]) extends Command

    final case class Average(numbers: List[Double]) extends Command

    final case class Min(numbers: List[Double]) extends Command

    final case class Max(numbers: List[Double]) extends Command

  }

  final case class ErrorMessage(value: String) {

    val message: String = s"Error: $value"
  }

  // Adjust `Result` and `ChangeMe` as you wish - you can turn Result into a `case class` and remove the `ChangeMe` if
  // you think it is the best model for your solution, or just have other `case class`-es implement `Result`
  final case class Result(command: Command, result: Double)

  def parseCommand(x: String): Either[ErrorMessage, Command] = {
    // Implementation hints:
    // You can use String#split, convert to List using .toList, then pattern match on:
    //   case x :: xs => ???
    x.toLowerCase.split(" +").toList match {
      case command :: args =>
        val ifArgsValidThen = ifArgsValid(args) _
        command match {
          case "divide" => ifArgsValidThen(argsDouble =>
            argsDouble match {
              case dividend :: divisor :: Nil => Right(Divide(dividend, divisor))
              case _ => Left(ErrorMessage(s"Unknown divide parameters: ${elements(argsDouble)}"))
            })
          case "sum" => ifArgsValidThen(argsDouble => Right(Sum(argsDouble)))
          case "average" => ifArgsValidThen(argsDouble => Right(Command.Average(argsDouble)))
          case "min" => ifArgsValidThen(argsDouble => Right(Command.Min(argsDouble)))
          case "max" => ifArgsValidThen(argsDouble => Right(Command.Max(argsDouble)))
          case unknownCommand => Left(ErrorMessage(s"Unknown command: $unknownCommand"))
        }
    }
    // Consider how to handle extra whitespace gracefully (without errors).
  }

  // should return an error (using `Left` channel) in case of division by zero and other
  // invalid operations
  def calculate(x: Command): Either[ErrorMessage, Result] = {
    x match {
      case Divide(_, 0) => Left(ErrorMessage("Division by zero"))
      case d: Divide => Right(Result(d, d.dividend / d.divisor))
      case s: Sum => Right(Result(s, s.numbers.sum))
      case avg: Average => Right(Result(avg, avg.numbers.sum / avg.numbers.size))
      case min: Min => Right(Result(min, min.numbers.min))
      case max: Max => Right(Result(max, max.numbers.max))
    }
  }

  def renderResult(x: Result): String = {
    val resultAsString = toString(x.result)
    x.command match {
      case d: Divide => s"${toString(d.dividend)} divided by ${toString(d.divisor)} is $resultAsString"
      case s: Sum => s"the sum of ${elements(s.numbers)} is $resultAsString"
      case avg: Average => s"the average of ${elements(avg.numbers)} is $resultAsString"
      case min: Min => s"the minimum of ${elements(min.numbers)} is $resultAsString"
      case max: Max => s"the maximum of ${elements(max.numbers)} is $resultAsString"
    }
  }

  def process(x: String): String = {
    import cats.implicits._
    // the import above will enable useful operations on Either-s such as `leftMap`
    // (map over the Left channel) and `merge` (convert `Either[A, A]` into `A`),
    // but you can also avoid using them using pattern matching.
    (for {
      comm <- parseCommand(x).leftMap(_.message)
      res <- calculate(comm).leftMap(_.message)
    } yield renderResult(res)).merge
  }

  // This `main` method reads lines from stdin, passes each to `process` and outputs the return value to stdout
  def main(args: Array[String]): Unit = Source.stdin.getLines() map process foreach println

  def toString(d: Double): String = if (d.isValidInt) d.toInt.toString else d.toString

  def elements(list: List[Double]): String = {
    list match {
      case x :: Nil => toString(x)
      case x :: xs => s"${toString(x)} ${elements(xs)}"
    }
  }

  def ifArgsValid(args: List[String])(f: List[Double] => Either[ErrorMessage, Command]): Either[ErrorMessage, Command] = {
    val argsEither = parseArgs(args)
    argsEither match {
      case Right(argsDouble) => f(argsDouble)
      case Left(left) => Left(left)
    }
  }

  def parseArgs(args: List[String]): Either[ErrorMessage, List[Double]] = {
    val tryDoubles: Try[List[Double]] = Try(args.map(_.toDouble))
    tryDoubles match {
      case Failure(exception) => Left(ErrorMessage(exception.getMessage))
      case Success(value) => Right(value)
    }
  }
}

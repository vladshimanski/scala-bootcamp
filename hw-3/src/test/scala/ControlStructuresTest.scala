package com.evolution.scala.bootcamp.hw3

import ControlStructures._

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

class ControlStructuresTest extends AnyFlatSpec with ScalaCheckDrivenPropertyChecks {

  "divide 4 5" should "output \"4 divided by 5 is 0.8\"" in {
    process("divide 4 5") shouldEqual "4 divided by 5 is 0.8"
  }

  "sum 5 5 6 8.5" should "output \"the sum of 5 5 6 8.5 is 24.5\"" in {
    process("sum 5 5 6 8.5") shouldEqual "the sum of 5 5 6 8.5 is 24.5"
  }

  "average 4 3 8.5 4" should "output \"the average of 4 3 8.5 4 is 4.875\"" in {
    process("average 4 3 8.5 4") shouldEqual "the average of 4 3 8.5 4 is 4.875"
  }

  "min 4 -3 -17" should "output \"the minimum of 4 -3 -17 is -17\"" in {
    process("min 4 -3 -17") shouldEqual "the minimum of 4 -3 -17 is -17"
  }

  "max 4 -3 -17" should "output \"the maximum of 4 -3 -17 is 4\"" in {
    process("max 4 -3 -17") shouldEqual "the maximum of 4 -3 -17 is 4"
  }

  "abc" should "output \"Error: Unknown command: abc\"" in {
    process("abc") shouldEqual "Error: Unknown command: abc"
  }

  "divide 2 0" should "output \"Error: Division by zero\"" in {
    process("divide 2 0") shouldEqual "Error: Division by zero"
  }

  "divide 2" should "output \"Error: Unknown divide parameters: 2\"" in {
    process("divide 2") shouldEqual "Error: Unknown divide parameters: 2"
  }

  "divide 2 1 3" should "output \"Error: Unknown divide parameters: 2 1 3\"" in {
    process("divide 2 1 3") shouldEqual "Error: Unknown divide parameters: 2 1 3"
  }

  "divide 2 1 3 9.3" should "output \"Error: Unknown divide parameters: 2 1 3 9.3\"" in {
    process("divide 2 1 3 9.3") shouldEqual "Error: Unknown divide parameters: 2 1 3 9.3"
  }

  "" should "output \"Error: Unknown command: \"" in {
    process("") shouldEqual "Error: Unknown command: "
  }

}

package com.evolution.scala.bootcamp.hw1

import scala.annotation.tailrec
import scala.math.abs

object Main {

  // Homework. Implement functions that calculate https://en.wikipedia.org/wiki/Least_common_multiple and
  // https://en.wikipedia.org/wiki/Greatest_common_divisor for integers.

  def lcm(a: Int, b: Int): Int = {
    if (a == 0 && b == 0) {
      0
    } else {
      abs(a * b) / gcd(a, b)
    }
  }

  @tailrec
  def gcd(a: Int, b: Int): Int = {
    if (a == 0 || b == 0) {
      a + b
    } else {
      if (a > b) {
        gcd(a % b, b)
      }
      else {
        gcd(a, b % a)
      }
    }
  }

}

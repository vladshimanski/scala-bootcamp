package com.evolution.scala.bootcamp.hw1

import Main._

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

class MainSpec extends AnyFlatSpec with ScalaCheckDrivenPropertyChecks {

  "gcd" should "give 4 for 8 and 12" in {
    gcd(8, 12) shouldEqual 4
  }

  "gcd" should "give 6 for 54 and 24" in {
    gcd(54, 24) shouldEqual 6
  }

  "lcm" should "give 12 for 4 and 6" in {
    lcm(4, 6) shouldEqual 12
  }

  "lcm" should "give 10 for 10 and 5" in {
    lcm(10, 5) shouldEqual 10
  }

  "lcm" should "give 0 for 0 and 0" in {
    lcm(0, 0) shouldEqual 0
  }

}

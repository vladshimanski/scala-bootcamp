package com.evolution.scala.bootcamp.hw2

import ClassesAndTraits._

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

class ClassesAndTraitsTest extends AnyFlatSpec with ScalaCheckDrivenPropertyChecks {

  "square" should "have correct points" in {
    val s: Square = Square(Point2D(2, 2), Point2D(5, 4))
    s.x shouldEqual 3.5
    s.y shouldEqual 3
    s.p1 shouldEqual Point2D(2, 2)
    s.p2 shouldEqual Point2D(4.5, 1.5)
    s.p3 shouldEqual Point2D(5, 4)
    s.p4 shouldEqual Point2D(2.5, 4.5)
  }

  "square" should "have correct area" in {
    Square(Point2D(0, 0), Point2D(4, 4)).area shouldEqual 16
  }

  "square" should "be moved correct" in {
    Square(Point2D(2, 6), Point2D(8, 9)).move(1, 2) shouldEqual Square(Point2D(3, 8), Point2D(9, 11))
  }

  "sphere" should "calculate correct volume" in {
    Sphere(Point3D(1, 1, 1), 5).volume shouldEqual 4 * math.Pi * 5 * 5 * 5 / 3
  }

  "sphere" should "calculate correct surface are" in {
    Sphere(Point3D(1, 1, 1), 5).surfaceArea shouldEqual 4 * math.Pi * 5 * 5
  }

  "sphere" should "be moved correct" in {
    Sphere(Point3D(1, 1, 1), 5).move(4, 5, 1) shouldEqual Sphere(Point3D(5, 6, 2), 5)
  }

  "point3d" should "be moved correct" in {
    Point3D(2, 3, 4).move(4, 8, 0) shouldEqual Point3D(6, 11, 4)
  }

  "point2d" should "be moved correct" in {
    Point2D(2, 3).move(4, 8) shouldEqual Point2D(6, 11)
  }

  "triangle" should "have correct side lenghts" in {
    val t: Triangle = Triangle(Point2D(0, 0), Point2D(0, 5), Point2D(10, 0))
    t.a shouldEqual 5
    t.b shouldEqual math.sqrt(125)
    t.c shouldEqual 10
  }

  "triangle" should "have correct area" in {
    val t: Triangle = Triangle(Point2D(0, 0), Point2D(0, 5), Point2D(10, 0))
    t.area shouldEqual 25
  }

  "triangle" should "be moved correct" in {
    Triangle(Point2D(0, 0), Point2D(0, 5), Point2D(10, 0)).move(3, 8) shouldEqual
      Triangle(Point2D(3, 8), Point2D(3, 13), Point2D(13, 8))
  }

  "triangle" should "have correct perimeter" in {
    Triangle(Point2D(0, 0), Point2D(0, 5), Point2D(10, 0)).perimeter shouldEqual (15 + math.sqrt(125))
  }
}

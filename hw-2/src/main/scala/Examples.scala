package com.evolution.scala.bootcamp.hw2

import ClassesAndTraits._

object Examples {

  val shapes3D: List[Shape3D] = List(
    Point3D(1, 3, 4),
    Sphere(Point3D(1, 1, 1), 5)
  )
  val movedShaped3D: List[Shape3D] = shapes3D.map(_.move(1, 0, 0))
  val volumes: List[Double] = shapes3D.map(_.volume)

  val shapes2D: List[Shape2D] = List(
    Triangle(Point2D(1, 1), Point2D(0, 0), Point2D(-5, -10)),
    Circle(Point2D(2.5, 10), 11),
    Square(Point2D(0, 0), Point2D(10, 10))
  )
  val movedShaped2D: List[Shape2D] = shapes2D.map(_.move(1, 0))

}

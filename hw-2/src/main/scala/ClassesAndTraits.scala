package com.evolution.scala.bootcamp.hw2

object ClassesAndTraits {

  // Homework by 2021-02-03
  //
  // Add additional 2D shapes such as triangle and square.
  //
  // In addition to the 2D shapes classes, add also 3D shapes classes
  // (origin, point, sphere, cube, cuboid, 3D triangle - you can add
  // others if you think they are a good fit).
  //
  // Add method `area` to 2D shapes.
  //
  // Add methods `surfaceArea` and `volume` to 3D shapes.
  //
  // If some of the implementation involves advanced math, it is OK
  // to skip it (leave unimplemented), the primary intent of this
  // exercise is modelling using case classes and traits, and not math.

  sealed trait Located2D {
    def x: Double
    def y: Double
  }

  sealed trait Located3D extends Located2D {
    def z: Double
  }

  sealed trait Bounded2D {
    def minX: Double
    def maxX: Double
    def minY: Double
    def maxY: Double
  }

  sealed trait Bounded3D extends Bounded2D {
    def minZ: Double
    def maxZ: Double
  }

  trait Movable2D[A <: Shape2D] {
    self: A =>
    def move(dx: Double, dy: Double): A
  }

  trait Movable3D[A <: Shape3D] {
    self: A =>
    def move(dx: Double, dy: Double, dz: Double): A
  }

  sealed trait Shape2D extends Located2D with Bounded2D with Movable2D[Shape2D] {
    def area: Double
  }

  sealed trait Shape3D extends Located3D with Bounded3D with Movable3D[Shape3D] {
    def surfaceArea: Double
    def volume: Double
  }

  final case class Point2D(x: Double, y: Double) extends Shape2D {
    override def minX: Double = x
    override def maxX: Double = x
    override def minY: Double = y
    override def maxY: Double = y
    override def move(dx: Double, dy: Double): Point2D = Point2D(x + dx, y + dy)
    def lengthBetween(p: Point2D): Double =
      math.sqrt(math.pow(x - p.x, 2) + math.pow(y - p.y, 2))
    override def area: Double = 0
  }

  final case class Circle(center: Point2D, radius: Double) extends Shape2D {
    override def x: Double = center.x
    override def y: Double = center.y
    override def minX: Double = center.x - radius
    override def maxX: Double = center.x + radius
    override def minY: Double = center.y - radius
    override def maxY: Double = center.y + radius
    override def move(dx: Double, dy: Double): Circle = Circle(center.move(dx, dy), radius)
    override def area: Double = math.Pi * radius * radius
  }

  final case class Triangle(p1: Point2D, p2: Point2D, p3: Point2D) extends Shape2D {
    val a: Double = p1.lengthBetween(p2)
    val b: Double = p2.lengthBetween(p3)
    val c: Double = p3.lengthBetween(p1)
    val perimeter: Double = a + b + c
    override def minX: Double = p1.x.min(p2.x).min(p3.x)
    override def maxX: Double = p1.x.max(p2.x).max(p3.x)
    override def minY: Double = p1.y.min(p2.y).min(p3.y)
    override def maxY: Double = p1.y.max(p2.y).max(p3.y)
    override def x: Double = (p1.x + p2.x + p3.x) / 3
    override def y: Double = (p1.y + p2.y + p3.y) / 3
    override def move(dx: Double, dy: Double): Triangle = Triangle(p1.move(dx, dy), p2.move(dx, dy), p3.move(dx, dy))
    override def area: Double = {
      val p = perimeter / 2
      math.sqrt(p * (p - a) * (p - b) * (p - c))
    }
  }

  final case class Square(p1: Point2D, p3: Point2D) extends Shape2D {
    val p2: Point2D = Point2D(x + diff.y, y - diff.x)
    val p4: Point2D = Point2D(x - diff.y, y + diff.x)
    private[this] def diff: Point2D = Point2D(p3.x - x, p3.y - y)
    override def area: Double = math.pow(p1.lengthBetween(p2), 2)
    override def minX: Double = p1.x.min(p2.x).min(p3.x).min(p4.x)
    override def maxX: Double = p1.x.max(p2.x).max(p3.x).max(p4.x)
    override def minY: Double = p1.y.min(p2.y).min(p3.y).min(p4.y)
    override def maxY: Double = p1.y.max(p2.y).max(p3.y).max(p4.y)
    override def x: Double = (p1.x + p3.x) / 2
    override def y: Double = (p1.y + p3.y) / 2
    override def move(dx: Double, dy: Double): Square = Square(p1.move(dx, dy), p3.move(dx, dy))
  }

  final case class Point3D(x: Double, y: Double, z: Double) extends Shape3D {
    override def surfaceArea: Double = 0
    override def volume: Double = 0
    override def move(dx: Double, dy: Double, dz: Double): Point3D = Point3D(x + dx, y + dy, z + dz)
    override def minX: Double = x
    override def maxX: Double = x
    override def minY: Double = y
    override def maxY: Double = y
    override def minZ: Double = z
    override def maxZ: Double = z
  }

  final case class Sphere(center: Point3D, radius: Double) extends Shape3D {
    override def surfaceArea: Double = 4 * math.Pi * radius * radius
    override def volume: Double = 4 * math.Pi * radius * radius * radius / 3
    override def move(dx: Double, dy: Double, dz: Double): Sphere = Sphere(center.move(dx, dy, dz), radius)
    override def minX: Double = center.x - radius
    override def maxX: Double = center.x + radius
    override def minY: Double = center.y - radius
    override def maxY: Double = center.y + radius
    override def minZ: Double = center.z - radius
    override def maxZ: Double = center.z + radius
    override def x: Double = center.x
    override def y: Double = center.y
    override def z: Double = center.z
  }

}

name := "hw-4"

version := "0.1"

scalaVersion := "2.13.4"

idePackagePrefix := Some("com.evolution.scala.bootcamp.hw4")

val scalaTestVersion = "3.1.0.0-RC2"

libraryDependencies ++= Seq(
  "org.scalatestplus" %% "scalatestplus-scalacheck" % scalaTestVersion % Test,
)

package com.evolution.scala.bootcamp.hw4

import Collections._

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

import scala.util.Random

class CollectionsSpec extends AnyFlatSpec with ScalaCheckDrivenPropertyChecks {

  "runningSum" should "work with positive numbers" in {
    runningSum(Array(1, 2, 3, 4)) shouldEqual Array(1, 3, 6, 10)
  }

  it should "work on empty array" in {
    runningSum(Array()) shouldEqual Array()
  }

  it should "work with negative numbers" in {
    runningSum(Array(-1, -2, -3, -4)) shouldEqual Array(-1, -3, -6, -10)
  }

  "shuffle" should "work on empty array" in {
    shuffle(Array(), 0) shouldEqual Array()
  }

  it should "work" in {
    shuffle(Array(2, 5, 1, 3, 4, 7), 3) shouldEqual Array(2, 3, 5, 4, 1, 7)
  }

  "maximumWealth" should "work on empty array" in {
    maximumWealth(Array(Array())) shouldEqual 0
  }

  it should "work" in {
    maximumWealth(Array(Array(2, 8, 7), Array(7, 1, 3), Array(1, 9, 5))) shouldEqual 17
  }

  "kidsWithCandies" should "work" in {
    kidsWithCandies(Array(4, 2, 1, 1, 2), 1) shouldEqual Array(true, false, false, false, false)
  }

  "maxWidthOfVerticalArea" should "work 1" in {
    maxWidthOfVerticalArea(Array(Array(8, 7), Array(9, 9), Array(7, 4), Array(9, 4))) shouldEqual 1
  }

  it should "work 2" in {
    maxWidthOfVerticalArea(Array(Array(1, 1), Array(1, 2), Array(1, 3))) shouldEqual 0
  }

  "maxDepth" should "work" in {
    maxDepth("(1+(2*3)+((8)/4))+1") shouldEqual 3
  }

  "balancedStringSplit" should "work" in {
    balancedStringSplit("RLRRLLRLRL") shouldEqual 4
  }

  "matrixBlockSum" should "work" in {
    matrixBlockSum(Array(Array(1, 2, 3), Array(4, 5, 6), Array(7, 8, 9)), 1) shouldEqual
        Array(Array(12, 21, 16), Array(27, 45, 33), Array(24, 39, 28))
  }

  "min" should "work correctly on empty" in {
    min(Nil) shouldEqual None
  }

  "min" should "work correctly on non empty" in {
    min(Random.shuffle(1 to 100).toList) shouldEqual Some(1)
  }

  "scanLeft" should "work correctly on numbers" in {
    val numbers = (1 to 100).toList
    scanLeft(0)(numbers)(_ + _) shouldEqual numbers.scanLeft(0)(_ + _)
  }

  "scanLeft" should "work correctly on letters" in {
    val letters = ('a' to 'z').toList.map(_.toString)
    scanLeft("")(letters)(_ + _) shouldEqual letters.scanLeft("")(_ + _)
  }

  "count" should "pass" in {
    count("aaaabbbcca") shouldEqual List(('a', 4), ('b', 3), ('c', 2), ('a', 1))
  }

  "allEqual" should "work for lists which are all equal" in {
    allEqual(List("a", "a", "a", "a")) shouldBe true
  }

  "allEqual" should "work on 1 element list" in {
    allEqual(List("a")) shouldBe true
  }

  "allEqual" should "work for lists which are NOT all equal" in {
    allEqual(List("a", "a", "b", "a")) shouldBe false
  }

  "totalVegetableCost" should "be correct" in {
    totalVegetableCost shouldEqual 5012
  }

  "allSubSetsOfSizeN" should "work correctly on 2 from Set(1, 2, 3)" in {
    allSubsetsOfSizeN(Set(1, 2, 3), 2) shouldEqual Set(Set(1, 2), Set(2, 3), Set(1, 3))
  }

  it should "work correctly" in {
    def fact(num: Int): BigDecimal = {
      (1 to num).map(x => BigDecimal.valueOf(x.toLong)).foldLeft(BigDecimal.valueOf(1))((a, b) => a * b)
    }

    val set = (0 until 16).toSet
    (1 to 4) foreach { k =>
      val obtained = allSubsetsOfSizeN(set, k)
      val n = set.size
      val expectedSize = fact(n) / (fact(k) * fact(n - k))
      obtained.size shouldEqual expectedSize.toLong
      obtained.forall(_.size == k) shouldEqual true
    }
  }

  "sort considering equal values" should "be correct on example 1" in {
    val input = Map("a" -> 1, "b" -> 2, "c" -> 4, "d" -> 1, "e" -> 0, "f" -> 2, "g" -> 2)
    val expected = List(Set("e") -> 0, Set("a", "d") -> 1, Set("b", "f", "g") -> 2, Set("c") -> 4)
    val obtained = sortConsideringEqualValues(input)
    obtained shouldEqual expected
  }

  it should "be correct on example 2" in {
    val values = Set("a1", "a2", "b1", "c1", "c2", "d1").map { x =>
      x -> x.head.toInt
    }.toMap

    sortConsideringEqualValues(values) shouldEqual List(
      Set("a1", "a2") -> 'a'.toInt,
      Set("b1") -> 'b'.toInt,
      Set("c1", "c2") -> 'c'.toInt,
      Set("d1") -> 'd'.toInt,
    )
  }
}

package com.evolution.scala.bootcamp.hw4

object Collections {

  // https://leetcode.com/problems/running-sum-of-1d-array/
  def runningSum(nums: Array[Int]): Array[Int] = {
    nums.foldLeft(Array.emptyIntArray) { (acc, x) => acc :+ (x + acc.lastOption.getOrElse(0)) }
  }

  // https://leetcode.com/problems/shuffle-the-array
  def shuffle(nums: Array[Int], n: Int): Array[Int] = {
    nums.splitAt(n) match {
      case (firstPart, secondPart) => firstPart.zip(secondPart).flatMap { case (f, s) => Array(f, s) }
    }
  }

  // https://leetcode.com/problems/richest-customer-wealth
  def maximumWealth(accounts: Array[Array[Int]]): Int = {
    accounts.map(_.sum).max
  }

  // https://leetcode.com/problems/kids-with-the-greatest-number-of-candies/
  def kidsWithCandies(candies: Array[Int], extraCandies: Int): Array[Boolean] = {
    val maxCandies = candies.max
    candies.map(candy => candy + extraCandies >= maxCandies)
  }

  // https://leetcode.com/problems/widest-vertical-area-between-two-points-containing-no-points
  def maxWidthOfVerticalArea(points: Array[Array[Int]]): Int = {
    val xs: Array[Int] = points.map(_.head).distinct.sorted
    xs match {
      case Array.emptyIntArray => 0
      case Array(_) => 0
      case _ => xs.zip(xs.tail).map { case (x1, x2) => x2 - x1 }.max
    }
  }

  // https://leetcode.com/problems/maximum-nesting-depth-of-the-parentheses/
  def maxDepth(s: String): Int = {
    s.foldLeft((0, 0)) {
      case ((currDepth, maxDepth), curSymbol) => curSymbol match {
        case '(' => (currDepth + 1, maxDepth.max(currDepth + 1))
        case ')' => (currDepth - 1, maxDepth)
        case _ => (currDepth, maxDepth)
      }
    }._2
  }

  // https://leetcode.com/problems/split-a-string-in-balanced-strings
  def balancedStringSplit(s: String): Int = {
    s.foldLeft((0, 0, 0)) {
      case ((splittedNumber, l, r), currSymbol) => currSymbol match {
        case 'L' => if (l + 1 == r) (splittedNumber + 1, 0, 0) else (splittedNumber, l + 1, r)
        case 'R' => if (l == r + 1) (splittedNumber + 1, 0, 0) else (splittedNumber, l, r + 1)
        case _ => (splittedNumber, l, r)
      }
    }._1
  }

  // https://leetcode.com/problems/matrix-block-sum/
  //  Variant 1
  //  def matrixBlockSum(mat: Array[Array[Int]], K: Int): Array[Array[Int]] = {
  //    def answer(i: Int, j: Int): Int = {
  //      findElems(i, j).sum
  //    }
  //    def findElems(i: Int, j: Int): Array[Int] = {
  //      val minLine = i - K
  //      val maxLine = i + K
  //      val minRow = j - K
  //      val maxRow = j + K
  //      val inBorder = (line: Int, row: Int) => {
  //        minLine <= line && line <= maxLine && minRow <= row && row <= maxRow
  //      }
  //      mat.zipWithIndex.flatMap {
  //        case (ints, i) => ints.zipWithIndex.filter {
  //          case (elem, j) => inBorder(i, j)
  //        }.map(_._1)
  //      }
  //    }
  //
  //    mat.zipWithIndex.map {
  //      case (ints, i) => ints.zipWithIndex.map {
  //        case (_, j) => answer(i, j)
  //      }
  //    }
  //  }

  //  Variant 2
  def matrixBlockSum(mat: Array[Array[Int]], K: Int): Array[Array[Int]] = {
    val n = mat.length
    val m = mat(0).length

    def generatePairs(i: Int, j: Int): Array[(Int, Int)] = {
      (for {
        i1 <- (i - K).to(i + K)
        if i1 >= 0
        if i1 < n
        i2 <- (j - K).to(j + K)
        if i2 >= 0
        if i2 < m
      } yield (i1, i2)).toArray
    }

    def elems(pairs: Array[(Int, Int)]): Array[Int] = {
      pairs.map { case (i, j) => mat(i)(j) }
    }

    mat.zipWithIndex.map {
      case (ints, i) => ints.zipWithIndex.map {
        case (_, j) => elems(generatePairs(i, j)).sum
      }
    }
  }

  // try to implement min different ways (fold, reduce, recursion)
  def min(list: List[Int]): Option[Int] = {
    //    @tailrec
    //    def minInner(l: List[Int], min: Option[Int]): Option[Int] = {
    //      l match {
    //        case x :: xs => min match {
    //          case Some(value) => minInner(xs, Some(value.min(x))
    //          case None => minInner(xs, Some(x))
    //        }
    //        case Nil => min
    //      }
    //    }
    //    minInner(list, None)

    list match {
      case Nil => None
      case x :: xs => xs.foldLeft(Some(x)) { (acc: Option[Int], x: Int) =>
        acc match {
          case None => Some(x)
          case Some(value) => Some(value.min(x))
        }
      }
    }

    //    list match {
    //      case Nil => None
    //      case _ => Some(list.reduce((a, b) => a.min(b)))
    //    }
  }

  // Implement scanLeft (not using scans ofc)
  def scanLeft[T](zero: T)(list: List[T])(f: (T, T) => T): List[T] = {
    list.foldLeft((List(zero), zero)) {
      case ((l, lastElem), x) =>
        val newLastElem = f(lastElem, x)
        (newLastElem :: l, newLastElem)
    }._1.reverse
  }

  def count(s: String): List[(Char, Int)] = {
    s.foldLeft(List[(Char, Int)]()) {
      case (result, currLetter) =>
        result match {
          case (letter, count) :: tail if letter == currLetter => (currLetter, count + 1) :: tail
          case _ => (currLetter, 1) :: result
        }
    }.reverse
  }

  // Exercise. Write a function that checks if all values in a `List` are equal.
  // Think about what you think your function should return if `list` is empty, and why.
  def allEqual[T](list: List[T]): Boolean = {
    list.foldLeft(true) { case (acc, x) => acc && x == list.head }
  }

  val totalVegetableCost: Int = {
    val vegetablePrices = Map(
      "tomatoes" -> 4,
      "peppers" -> 5,
      "olives" -> 17,
    )
    val vegetableAmounts = Map(
      "tomatoes" -> 17,
      "peppers" -> 234,
      "olives" -> 32,
      "cucumbers" -> 323,
    )
    vegetableAmounts.map { case (veg, amount) => amount * vegetablePrices.getOrElse(veg, 10) }.sum
  }

  val totalVegetableWeights: Map[String, Int] = {
    val vegetableWeights = Map(
      ("pumpkins", 10),
      ("cucumbers", 20),
      ("olives", 2),
    )
    val vegetableAmounts = Map(
      "tomatoes" -> 17,
      "peppers" -> 234,
      "olives" -> 32,
      "cucumbers" -> 323,
    )
    vegetableWeights
        .filter { case (veg, _) => vegetableAmounts.contains(veg) }
        .map { case (veg, weight) => (veg, weight * vegetableAmounts.getOrElse(veg, 0)) }
  }

  // Exercise: Return a set with all subsets of the provided set `set` with `n` elements
  // For example, `allSubsetsOfSizeN(Set(1, 2, 3), 2) == Set(Set(1, 2), Set(2, 3), Set(1, 3))`.
  // Hints for implementation:
  //   - Handle the trivial case where `n == 1`.
  //   - For other `n`, for each `set` element `elem`, generate all subsets of size `n - 1` from the set
  //     that don't include `elem`, and add `elem` to them.
  def allSubsetsOfSizeN[A](set: Set[A], n: Int): Set[Set[A]] = {
    //    n match {
    //      case 1 => set.map(Set(_))
    //      case n => set.flatMap(elem =>
    //        allSubsetsOfSizeN(set - elem, n - 1)
    //            .map(innerSet => innerSet + elem))
    //    }
    n match {
      case 1 => set.map(Set(_))
      case _ =>
        for {
          elem <- set
          subsets <- allSubsetsOfSizeN(set - elem, n - 1)
        } yield subsets + elem
    }
  }

  // Implement a special sort which sorts the keys of a map (K) according to their associated
  // values (V).
  //
  // In case of "ties" (equal values) it should group these keys K into Set-s in the results.
  //
  // The input is a map where keys (K) are the values to be sorted and values are their associated numeric
  // values.
  //
  // The output is a list (in ascending order according to the associated `Int` values) of tuples of `Set`-s
  // with values from K, and the associated value V for these values in the `Set`.
  //
  // For example:
  //
  // Input `Map("a" -> 1, "b" -> 2, "c" -> 4, "d" -> 1, "e" -> 0, "f" -> 2, "g" -> 2)` should result in
  // output `List(Set("e") -> 0, Set("a", "d") -> 1, Set("b", "f", "g") -> 2, Set("c") -> 4)`.
  def sortConsideringEqualValues[T](map: Map[T, Int]): List[(Set[T], Int)] = {
    map.toList
        .sortWith { case (elem1, elem2) => elem1._2 >= elem2._2 }
        .foldLeft(List[(Set[T], Int)]()) { case (acc, (currLetter, currNumber)) =>
          acc match {
            case (letter, number) :: tail if number == currNumber => (letter + currLetter, currNumber) :: tail
            case _ => (Set(currLetter), currNumber) :: acc
          }
        }
  }

}
